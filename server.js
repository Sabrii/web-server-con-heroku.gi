const express = require('express')
const app = express();
const hbs=require('hbs');
const server=app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
     console.log("Express is working on port " + port);
    
})

app.use(express.static(__dirname + '/views'));
hbs.registerPartials(__dirname + '/views/parciales');
app.set('view engine', 'hbs');
 
app.get('/', function (req, res) {

    res.render('home.hbs')

});
app.get('/about', function (req, res) {

    res.render('about.hbs')

});
app.get('/home', function (req, res) {

    res.render('home.hbs')

});
 

 
// app.listen(port, function ()
// {
//     console.log(`Escuchando en el puerto ${port}`);
// }
// )